package ru.tfs.cloud.microservices.customer.client;

import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ru.tfs.cloud.microservices.customer.dto.OrderDto;

@FeignClient(name = "order", fallback = OrderFallback.class)
public interface OrderClient {

    @GetMapping("/api/order/{customerId}")
    List<OrderDto> getOrders(@PathVariable long customerId);

}

@Slf4j
@Component
class OrderFallback implements OrderClient {

    @Override
    public List<OrderDto> getOrders(long customerId) {
        log.warn("fallback called...");
        return List.of();
    }
}
