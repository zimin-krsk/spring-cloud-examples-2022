package ru.tfs.cloud.microservices.order.service;

import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.tfs.cloud.microservices.order.dto.OrderDto;
import ru.tfs.cloud.microservices.order.repository.OrdersRepository;

@Slf4j
@Service
@Profile("!circuit-breaker")
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {
    private final OrdersRepository ordersRepository;

    @Override
    public List<OrderDto> getCustomerOrders(long customerId) {
        log.info("get orders by customer id {}", customerId);
        var customerOrders = ordersRepository.findByCustomerId(customerId)
                .stream()
                .map(it -> new OrderDto(it.getId(), it.getItem(), it.getCount()))
                .collect(Collectors.toList());
        log.info("found {} customer orders", customerOrders.size());
        return customerOrders;
    }

}
