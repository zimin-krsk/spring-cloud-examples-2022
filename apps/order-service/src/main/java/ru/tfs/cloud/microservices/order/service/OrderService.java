package ru.tfs.cloud.microservices.order.service;

import java.util.List;
import ru.tfs.cloud.microservices.order.dto.OrderDto;

public interface OrderService {
    List<OrderDto> getCustomerOrders(long customerId);
}
